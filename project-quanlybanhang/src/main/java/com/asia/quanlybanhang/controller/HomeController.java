package com.asia.quanlybanhang.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping({"/home", "/"})
public class HomeController {

    @GetMapping
    public String homePage(ModelMap modelMap, HttpSession httpSession){

        String userName = (String) httpSession.getAttribute("username");

        modelMap.addAttribute("userName", userName);

        return "home";
    }
}
