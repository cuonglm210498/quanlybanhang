package com.asia.quanlybanhang.controller;

import com.asia.quanlybanhang.entity.User;
import com.asia.quanlybanhang.provider.JwtTokenProvider;
import com.asia.quanlybanhang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;

@Controller
@RequestMapping("/login")
@SessionAttributes("username")
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @GetMapping
    public String loginPage() {
        return "login";
    }

    @PostMapping()
    public String checkLogin(HttpServletResponse response, @RequestParam String username,
                             @RequestParam String password,
                             ModelMap modelMap) throws NoSuchAlgorithmException {

/*        String pass = PasswordHasher.hash(password);

        boolean check = userService.checkLogin(username, pass);

        modelMap.addAttribute("username", username); //luu username vao trong session, su dung @SessionAttributes

        if (check){
            User user = userService.findUserByUsernameAndPassword(username, pass);
            if (userService.findRoleById(user.getId()).equals("ADMIN")){
                return "redirect:/products";
            }
            return "redirect:/home";
        }else{
            modelMap.addAttribute("logResult", "Đăng nhập thất bại");
            return "login";
        }*/

        User user = userService.findByUsername(username);
        if(user == null){
            modelMap.addAttribute("logResult", "Tên tài khooản không tồn tại !");
            return "login";
        }
        if(!passwordEncoder.matches(password, user.getPassword())){
            modelMap.addAttribute("logResult", "Mật khẩu ko chính xác !");
            return "login";
        }

        Cookie cookie = new Cookie("access_token", jwtTokenProvider.generateToken(user.getId()));
        response.addCookie(cookie);

        modelMap.addAttribute("username", username); //luu username vao trong session, su dung @SessionAttributes

        return "redirect:/home";
    }

}
