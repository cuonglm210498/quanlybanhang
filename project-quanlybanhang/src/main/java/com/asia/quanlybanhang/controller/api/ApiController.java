package com.asia.quanlybanhang.controller.api;

import com.asia.quanlybanhang.entity.Products;
import com.asia.quanlybanhang.service.ProductService;
import com.asia.quanlybanhang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@SessionAttributes("username")
public class ApiController {

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

//    @PostMapping("/check-login")
//    @ResponseBody
//    public String checkLogin(@RequestParam String username,
//                             @RequestParam String password,
//                             ModelMap modelMap){
//
//        boolean check = userService.checkLogin(username, password);
//
//        if (check){
//            System.out.println("Dang nhap thanh cong");
//        }else{
//            System.out.println("Dang nhap that bai");
//        }
//
//        modelMap.addAttribute("username", username); //luu username vao trong session, su dung @SessionAttributes
//
//        return String.valueOf(check);
//    }

//    @GetMapping("/search")
//    public ResponseEntity<?> searchProduct(String keyword){
//        List<Products> list = productService.searchProducts(keyword);
//
//        return ResponseEntity.ok(list);
//    }

//    @GetMapping("/show-products")
//    public Output showProduct(@RequestParam int page,
//                              @RequestParam int limit){
//
//        Output result = new Output();
//        result.setPage(page);
//
//        Pageable pageable = PageRequest.of(page - 1, limit);
//
//        result.setListResult(productService.getAllProducts(pageable));
//
//        result.setTotalPage((int) Math.ceil((double) (productService.totalItem()) / limit));
//
//        return result;
//    }
}