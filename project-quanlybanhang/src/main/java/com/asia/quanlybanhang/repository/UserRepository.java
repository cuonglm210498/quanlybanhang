package com.asia.quanlybanhang.repository;

import com.asia.quanlybanhang.entity.User;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT * FROM users WHERE username = ?1 AND password = ?2", nativeQuery = true)
    User findUserByUsernameAndPassword(String userName, String password);

    @Query(value = "SELECT r.name FROM role r join permission p on r.id = p.role_id WHERE user_id = ?1", nativeQuery = true)
    String findRoleById(Long id);

    User findByUsername(String username);

    User findUserById(Long id);
}
