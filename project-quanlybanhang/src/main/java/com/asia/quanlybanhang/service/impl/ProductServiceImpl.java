package com.asia.quanlybanhang.service.impl;

import com.asia.quanlybanhang.entity.Products;
import com.asia.quanlybanhang.repository.ProductRepository;
import com.asia.quanlybanhang.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Products> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public void saveProduct(Products products) {
        productRepository.save(products);
    }

    @Override
    public void deleteProducts(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Optional<Products> findProductById(Long id) {
        return productRepository.findById(id);
    }

    @Override
    public List<Products> searchProducts(String keyword) {
        if (keyword != null){
            return productRepository.searchProductByKeyWord(keyword);
        }
        return productRepository.findAll();
    }

    @Override
    public Page<Products> findProductsPaginated(int pageNo, int pageSize) {

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, Sort.by("id").descending());
        return productRepository.findAll(pageable);
    }

    @Override
    public Page<Products> searchProductsPaginated(String keyword, int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, Sort.by("id").descending());
        if (keyword != null){
            return productRepository.findProductsByNameContaining(keyword, pageable);
        }
        return  productRepository.findAll(pageable);
    }
}
