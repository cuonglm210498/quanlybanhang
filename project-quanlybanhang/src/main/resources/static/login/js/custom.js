$(document).ready(function (){
    $('#btnLogin').click(function (){
        let userName = $('#username').val();
        let password = $('#password').val();
        $.ajax({
            url:"/api/check-login",
            type:"POST",
            data:{
                username:userName,
                password:password
            },
            success: function (value){
                if (value == "true"){
                    duongDanhienTai = window.location.href;
                    duongDan = duongDanhienTai.replace("login", "admin");
                    window.location = duongDan;
                }else{
                    $('#logResult').text("Đăng nhập thất bại");
                }
            }
        })
    })
})